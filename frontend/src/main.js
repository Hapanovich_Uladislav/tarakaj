import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import './registerServiceWorker';
import 'vue-awesome/icons/quote-left';
import 'vue-awesome/icons/quote-right';
import 'vue-awesome/icons/arrow-up';
import Icon from 'vue-awesome/components/Icon';

Vue.component('v-icon', Icon);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#preloader');

import Vue from 'vue';
import Router from 'vue-router';
import Meta from 'vue-meta';

Vue.use(Router);
Vue.use(Meta);

export default new Router({
  mode: 'history',
  base: '/',
  scrollBehavior(to, from, savedPosition) {
    return {
      x: 0,
      y: 0
    };
  },
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import(/* webpackChunkName: "home" */ './pages/Home.vue')
    },
    {
      path: '/services',
      name: 'services',
      component: () => import(/* webpackChunkName: "services" */ './pages/Services.vue')
    },
    {
      path: '/car-park',
      name: 'carPark',
      component: () => import(/* webpackChunkName: "car-park" */ './pages/CarPark.vue')
    },
    {
      path: '/reviews',
      name: 'reviews',
      component: () => import(/* webpackChunkName: "reviews" */ './pages/Reviews.vue')
    },
    {
      path: '/contacts',
      name: 'contacts',
      component: () => import(/* webpackChunkName: "contacts" */ './pages/Contacts.vue')
    },
    {
      path: '/not-found',
      name: 'notFound',
      component: () => import(/* webpackChunkName: "not-found" */ './pages/NotFound.vue')
    },
    {
      path: '*',
      redirect: 'notFound'
    }
  ]
});

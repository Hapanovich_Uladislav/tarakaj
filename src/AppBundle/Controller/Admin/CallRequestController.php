<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\CallRequest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Response;

class CallRequestController extends Controller
{
    /**
     * @Secure(roles={"ROLE_SUPER_ADMIN"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request): Response
    {
        $paginator = $this->get('knp_paginator');
        $callRequests = $this->getDoctrine()
            ->getRepository(CallRequest::class)
            ->getAllCallRequestsQuery();
        $pagination = $paginator->paginate($callRequests, $request->get('page', 1), 20);

        return $this->render('@app/Home/index.html.twig', [
            'callRequests' => $pagination
        ]);
    }
}

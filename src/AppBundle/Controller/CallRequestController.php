<?php

namespace AppBundle\Controller;

use AppBundle\Entity\CallRequest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class CallRequestController extends Controller
{
    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function createAction(Request $request): JsonResponse
    {
        $reviewJson = \json_decode($request->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $callRequest = new CallRequest($reviewJson['name'], $reviewJson['phone']);

        $em->persist($callRequest);
        $em->flush();

        return new JsonResponse(true);
    }
}

<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\CallRequest;
use AppBundle\Entity\Review;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends Controller
{
    public function callListAction()
    {
        $calls = $this->getDoctrine()->getRepository(CallRequest::class)->findBy([
            'isActive' => false
        ]);

        return $this->render('@app/Home/index.html.twig', [
            'calls' => $calls
        ]);
    }

    public function callConfirmAction(CallRequest $callRequest)
    {
        $callRequest->setIsActive(true);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('admin_call_requests');
    }

    public function callUnconfirmAction(CallRequest $callRequest)
    {
        $callRequest->setIsActive(false);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('admin_call_requests');
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function reviewListAction(Request $request): Response
    {
        $paginator = $this->get('knp_paginator');
        $reviews = $this->getDoctrine()
            ->getRepository(Review::class)
            ->getAllReviewsQuery();
        $pagination = $paginator->paginate($reviews, $request->get('page', 1), 20);

        return $this->render('@app/Home/reviews.html.twig', [
            'reviews' => $pagination
        ]);
    }

    public function reviewConfirmAction(Review $review)
    {
        $review->setIsActive(true);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('admin_reviews');
    }

    public function reviewUnconfirmAction(Review $review)
    {
        $review->setIsActive(false);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('admin_reviews');
    }

    public function reviewRemoveAction(Review $review)
    {
        $review->setIsRemove(true);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('admin_reviews');
    }

    public function indexAction()
    {
        return $this->render('@desktop/index.html');
    }
}

<?php

namespace AppBundle\Controller;

use AppBundle\Entity\BotSubscriber;
use AppBundle\Entity\User;
use AppBundle\Repository\UserRepository;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Form\Factory\FactoryInterface;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class RegistrationController extends Controller
{
    /**
     * @param Request     $request
     * @param string|null $hash
     *
     * @return null|RedirectResponse|Response
     */
    public function registerAction(Request $request, string $hash = null)
    {
        /** @var $formFactory FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');
        /** @var $userManager UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        /** @var User $user */
        $user = $userManager->createUser();

        if ($hash) {
            $parentUser = $this->getDoctrine()->getRepository(User::class)->getUserByHash($hash);

            if (!$parentUser) {
                throw new NotFoundHttpException('It is not valid link');
            }

            $user->setParentUser($parentUser);
        }

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $event = new FormEvent($form, $request);

            if ($form->isValid()) {
                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);
                $userManager->updateUser($user);

                $this->container->get('logger')->info(
                    sprintf("New user registration: %s", $user)
                );

                if (null === $response = $event->getResponse()) {
                    $url = $this->generateUrl('homepage');
                    $response = new RedirectResponse($url);
                }

                $confirmationUrl = $this->generateUrl(
                    'registration_confirmation',
                    ['hash' => $user->getHash()],
                    UrlGeneratorInterface::ABSOLUTE_URL
                );

                $this->get('email_sender')->sendRegistrationConfirmation($user, $confirmationUrl);

                return $response;
            }

            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_FAILURE, $event);

            if (null !== $response = $event->getResponse()) {
                return $response;
            }
        }

        return $this->render('@app/Register/register.html.twig', [
            'form' => $form->createView(),
            'errors' => $form->getErrors(true)
        ]);
    }

    /**
     * @param User $user
     *
     * @return RedirectResponse
     */
    public function confirmAction(User $user): RedirectResponse
    {
        if ($user->isEnabled()) {
            throw new NotFoundHttpException('It is not valid link');
        }

        $user->activate();

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        $this->get('user_login_helper')->autoLoginUser($user, UserInterface::ROLE_DEFAULT);

        return $this->redirectToRoute('homepage');
    }
}
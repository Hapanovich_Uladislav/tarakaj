<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Review;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ReviewController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function indexAction(): JsonResponse
    {
        return new JsonResponse(
            $this->getDoctrine()->getManager()->getRepository(Review::class)->getAllActiveReviews()
        );
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function createAction(Request $request): JsonResponse
    {
        $reviewJson = \json_decode($request->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $review = new Review($reviewJson['name'], $reviewJson['email'], $reviewJson['text']);

        $em->persist($review);
        $em->flush();

        return new JsonResponse(true);
    }
}

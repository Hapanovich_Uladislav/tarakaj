<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class TelegramController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function webhookAction(): JsonResponse
    {
        $this->get('telegram_manager')->processRequest();

        return new JsonResponse([]);
    }
}

<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\IsTrue;

class RegistrationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->remove('username')
            ->add('email', EmailType::class, [
                    'required' => true,
                    'label' => false,
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'form.field.username'
                    ],
                    'translation_domain' => 'FOSUserBundle'
                ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'first_options' => [
                    'label' => false,
                    'attr' => [
                        'placeholder' => 'form.field.password',
                        'minlength' => 6,
                        'maxlength' => 25,
                        'class' => 'form-control'
                    ]
                ],
                'second_options' => [
                    'label' => false,
                    'attr' => [
                        'placeholder' => 'form.field.password_confirmation',
                        'minlength' => 6,
                        'maxlength' => 25,
                        'class' => 'form-control'
                    ]
                ],
                'invalid_message' => 'fos_user.password.mismatch',
                'translation_domain' => 'FOSUserBundle'
            ])
            ->add('terms', CheckboxType::class, [
                'mapped' => false,
                'label' => 'form.field.terms',
                'attr' => ['class' => 'checkbox'],
                'constraints' => new IsTrue([
                    'message' => 'Для регистрации примите условия соглашени'
                ]),
                'required' => false,
                'translation_domain' => 'FOSUserBundle'
            ]);
    }

    /**
     * @return string
     */
    public function getParent(): string
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    /**
     * @return string
     */
    public function getBlockPrefix(): string
    {
        return 'app_user_registration';
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->getBlockPrefix();
    }
}
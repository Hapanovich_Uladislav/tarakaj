<?php

namespace AppBundle\Helper;

use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class UserLoginHelper
{
    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * @var Session
     */
    private $session;

    /**
     * @param TokenStorage $tokenStorage
     * @param Session      $session
     */
    public function __construct(TokenStorage $tokenStorage, Session $session)
    {
        $this->tokenStorage = $tokenStorage;
        $this->session = $session;
    }

    /**
     * @param \AppBundle\Entity\User   $user
     * @param string $role
     */
    public function autoLoginUser(User $user, string $role): void
    {
        $token = new UsernamePasswordToken($user, null, 'main', [$role]);
        $this->tokenStorage->setToken($token);
        $this->session->set('_security_main', serialize($token));
    }
}
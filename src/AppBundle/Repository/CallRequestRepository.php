<?php

namespace AppBundle\Repository;

use Doctrine\ORM\Query;

class CallRequestRepository extends \Doctrine\ORM\EntityRepository
{
    public function getAllActiveReviews(): array
    {
        $qb = $this->createQueryBuilder('cr');

        return $qb
            ->select('cr.id', 'cr.name', 'cr.text')
            ->where($qb->expr()->eq('cr.isActive', ':isActive'))
            ->setParameter('isActive', true)
            ->getQuery()
            ->getResult();
    }

    public function getAllCallRequestsQuery(): Query
    {
        $qb = $this->createQueryBuilder('cr');

        return $qb
            ->orderBy('cr.createdAt', 'DESC')
            ->getQuery();
    }
}

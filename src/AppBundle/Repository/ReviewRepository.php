<?php

namespace AppBundle\Repository;

use Doctrine\ORM\Query;

class ReviewRepository extends \Doctrine\ORM\EntityRepository
{
    public function getAllActiveReviews(): array
    {
        $qb = $this->createQueryBuilder('r');

        return $qb
            ->select('r.id', 'r.name', 'r.text')
            ->where($qb->expr()->eq('r.isActive', ':isActive'))
            ->setParameter('isActive', true)
            ->getQuery()
            ->getResult();
    }

    public function getAllReviewsQuery(): Query
    {
        $qb = $this->createQueryBuilder('r');

        return $qb
            ->where($qb->expr()->eq('r.isRemove', ':isRemove'))
            ->setParameter('isRemove', false)
            ->orderBy('r.createdAt', 'DESC')
            ->getQuery();
    }
}
